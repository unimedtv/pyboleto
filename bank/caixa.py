# -*- coding: utf-8 -*-
from pyboleto.data import BoletoData, CustomProperty


class BoletoCaixa(BoletoData):
    '''
        Gera Dados necessários para criação de boleto para o banco Caixa
        Economica Federal

    '''

    #conta_cedente = CustomProperty('conta_cedente', 11)
    conta_cedente = CustomProperty('conta_cedente', 3)
    '''
        Este numero tem o inicio fixo
        Carteira SR: 80, 81 ou 82
        Carteira CR: 90 (Confirmar com gerente qual usar)

    '''

    #Alterado por Amaury Goncalves - 19/01/2018
    
    #nosso_numero = CustomProperty('nosso_numero', 10)
    nosso_numero = CustomProperty('nosso_numero', 17)

    def __init__(self):
        super(BoletoCaixa, self).__init__()

        self.codigo_banco = "104"
        self.local_pagamento = "Preferencialmente nas Casas Lotéricas e \
Agências da Caixa"
        self.logo_image = "logo_bancocaixa.jpg"
        self.logo_image_unimed = "logoUnimed.jpg"

    @property
    def dv_nosso_numero(self):
        resto2 = self.modulo11(self.nosso_numero.split('-')[0], 9, 1)
        digito = 11 - resto2
        if digito == 10 or digito == 11:
            dv = 0
        else:
            dv = digito
        return dv

    @property
    def campo_livre(self):
        #Alterado por Amaury Goncalves - 19/01/2018

        # content = "%10s%4s%11s" % (self.nosso_numero,
        #                            self.agencia_cedente,
        #                            self.conta_cedente.split('-')[0])
        content = "%6s%1s%3s%1s%3s%1s%9s" % (self.conta_cedente.split('-')[0],
                                   self.conta_cedente.split('-')[1],
                                   self.nosso_numero[2:5],
                                   self.nosso_numero[0],
                                   self.nosso_numero[5:8],
                                   self.nosso_numero[1],
                                   self.nosso_numero[8:17])
        content = content+str(self.modulo11(content))
        return content

    def format_nosso_numero(self):
        return self.nosso_numero + '-' + str(self.dv_nosso_numero)

    def get_linha_digitavel(self):
        return self.linha_digitavel
